﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Reflection;
using System.Collections;

namespace OtusReflection
{
    class DeserializeAnyClass
    {
        public dynamic Deserialize(FilePath filePath)
        {
            var list = filePath.SplitPathForCVS();
            dynamic expando = new ExpandoObject();
            var expandoDict = (IDictionary<string, object>)expando;
            foreach (var field in list)
            {
                expandoDict.Add(field[0], field[1]);
            }

            return expandoDict;
        }
    }
}
