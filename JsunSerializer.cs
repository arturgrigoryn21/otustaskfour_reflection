﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.IO;

namespace OtusReflection
{
    class JsunSerializer : ISerializer
    {
        public void Serialize(F obj, FilePath filePath)
        {
            string json = JsonSerializer.Serialize(obj);
            File.WriteAllText(filePath.Path, json);
        }
        public F Deserialize(FilePath filePath)
        {
            var str = File.ReadAllText(filePath.Path);
            F fooz = JsonSerializer.Deserialize<F>(str);
            return fooz;
        }
    }
}
