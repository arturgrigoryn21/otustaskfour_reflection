﻿using System;
using System.Reflection;
using System.Text.Json;
using System.Diagnostics;



namespace OtusReflection
{
    class Program
    {
        static string c_path = "c_saves.txt";
        static string j_path = "j_saves.txt";
        static void Main(string[] args)
        {
            F fooz = new F();
            Context context = new Context();
            var timer = new Stopwatch();

            var c_ser = context.SerializeTimer(1000, fooz, new FilePath(c_path), new CsvSerializer());
            var j_ser = context.SerializeTimer(1000, fooz, new FilePath(j_path), new JsunSerializer());
            var c_des = context.DeserializeTimer(1000, new FilePath(c_path), new CsvSerializer());
            var j_des = context.DeserializeTimer(1000, new FilePath(j_path), new JsunSerializer());

            timer.Start();
            Console.WriteLine("Сериализуемый объект new F() { i1 = 1; i2 = 2; i3 = 3; i4 = 4; i5 = 5; } ");
            Console.WriteLine("Колличество итераций: 1000");
            Console.WriteLine($"Сериализация с рефлексией :  {c_ser} ");
            Console.WriteLine($"Сериализация System.Text.Json:  {j_ser} ");
            Console.WriteLine($"Десериализация с рефлексией :  {c_des} ");
            Console.WriteLine($"Десериализация System.Text.Json:  {j_des} ");
            timer.Stop();

            //Task 6
            Console.WriteLine($"Вывод текста на консоль: {timer.ElapsedMilliseconds} мс.");


            //Task 9,10
            DeserializeAnyClass deserialize = new DeserializeAnyClass();
            FilePath filePath = new FilePath(c_path);
            timer.Start();
            deserialize.Deserialize(filePath);
            timer.Stop();
            Console.WriteLine($"Время на десериализацию любого класса: {timer.ElapsedMilliseconds} мс.");
        }
    }
}
