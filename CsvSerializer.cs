﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

namespace OtusReflection
{
    class CsvSerializer : ISerializer
    {
        public void Serialize(F obj, FilePath filePath)
        {
            StringBuilder str = new StringBuilder();
            var type = obj.GetType();

            int count=0;
            foreach (var el in type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                count++;
                str.Append($"{el.Name}:{el.GetValue(obj)}");
                if (count < type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance).Length)
                {
                    str.Append(";");
                }
            }

            File.WriteAllText(filePath.Path, str.ToString());
        }

        public F Deserialize(FilePath filePath)
        {
            var list = filePath.SplitPathForCVS();
            var type = typeof(F);
            F fooz = new F();
            foreach (var el in list)
            {
                var field = type.GetField(el[0], BindingFlags.NonPublic | BindingFlags.Instance);
                
                field.SetValue(fooz, Convert.ToInt32(el[1]));
            }

            return fooz;
        }
    }

    
}
