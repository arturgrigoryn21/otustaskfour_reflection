﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace OtusReflection
{
    class Context
    {
        Stopwatch timer;
        public string SerializeTimer(int count, F obj, FilePath filePath, ISerializer serializer)
        {
            timer = new Stopwatch();
            timer.Start();
            for(int k=0;k<count;k++)
            {
                serializer.Serialize(obj, filePath);
            }
            timer.Stop();
            return String.Format( $"{timer.ElapsedMilliseconds} мс.");
        }

        public string DeserializeTimer(int count, FilePath filePath, ISerializer serializer)
        {
            timer = new Stopwatch();
            timer.Start();
            for(int k = 0; k<count; k++)
            {
                serializer.Deserialize(filePath);
            }
            timer.Stop();
            return String.Format($"{timer.ElapsedMilliseconds} мс.");
        }
    }
}
