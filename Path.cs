﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace OtusReflection
{
    class FilePath
    {
        string path;

        public string Path
        {
            get => path;
        }

        public FilePath(string path)
        {
            this.path = path;
        }

        public List<string[]> SplitPathForCVS()
        {
            var str = File.ReadAllText(path);            
            var list = str.Split(new char[] { ';' });
            List<string[]> list2 = new List<string[]>();

            foreach(var el in list)
            {
                list2.Add(el.Split(new char[] { ':' }));
            }
            return list2;
        }
    }
}
