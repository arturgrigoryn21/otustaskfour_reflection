﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusReflection
{
    interface ISerializer
    {
        void Serialize(F obj, FilePath filePath);
        F Deserialize(FilePath filePath);
    }
}
